const { createLogger, format, transports } = require('winston');
const { combine, timestamp, label, prettyPrint } = format;
const express = require('express');
const app = express();
var morgan = require('morgan');
var logdemo = require('./src/logdemo');

const logger = createLogger({
    level:'info',
    //format:winston.format.json(),
    format:combine(        
    label({label:'demo level'}),
    timestamp(),
    prettyPrint()),
    defaultMeta:{service:'user-service'},
    transports:[
        new transports.File({filename:'error.log',level:'error'}),
        new transports.File({filename:'combined.log'}),
    ]
});
logger.stream = {
    write: function(message, encoding) {
      // use the 'info' log level so the output will be picked up by both transports (file and console)
      logger.info(message);
    },
  };
if (process.env.NODE_ENV !== 'production') {
    logger.add(new transports.Console({
      format: format.simple()
    }));
  }
  app.use(morgan('combined', { stream: logger.stream }));
  global.logger = logger;
  logdemo(app);
  app.listen(4500);
  logger.error("Hello log this is first log");